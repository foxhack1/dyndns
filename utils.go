package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"
)

const appCompany = "Castle Industries"
const appUrl = "http://castle.industries"
const appName = "CI-DDNS"
const appVersion = "v1.1"

const getIpUrl = "https://icanhazip.com"
const getIpUrl2 = "http://ip1.dynupdate.no-ip.com"
const getIpUrl3 = "http://ip4.castle.industries"

// filesize returns the size of a file in bytes
func filesize(filename string) int64 {
	file, err := os.Stat(filename)
	if err != nil {
		log.Println(err)
		return 0
	}

	return file.Size()
}

// createFile creates an empty file.
func createFile(filename string) {
	fh, _ := os.Create(filename)
	fh.Close()
}

// writeToFile writes the data to a file.
// Expects a string argument, will cause a fatal error if data is not written.
func writeToFile(filename string, data string) {
	err := ioutil.WriteFile(filename, []byte(data), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}

// readFile returns the contents of a file
func readFile(filename string) string {
	c, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Printf("Error reading %s\n", filename)
	}

	return string(c)
}

// getLastIP looks for the last IP Address in the database
// If the record doesn't exist, it will be created.
func getLastIP() string {
	ip, _ := readIP("currentIP")

	if ip == "" {
		writeIP("currentIP", currentIP)
	}

	return ip
}

// siteReachable tests to see if a website is reachable on port 80 or port 443
// returns bool
func siteReachable(host string) bool {
	baseUrl := strings.Replace(strings.Replace(host, "http://", "", 1), "https://", "", 1)
	port := "80"
	if strings.Contains(host, "https") {
		port = "443"
	}

	timeout := time.Duration(1 * time.Second)
	_, err := net.DialTimeout("tcp", baseUrl+":"+port, timeout)
	if err != nil {
		return false
	}

	return true
}

// httpRequest is a generic function used to make HTTP calls. A FQDN string, and HTTP request method is expected.
// Returns a the body of the http response as a string.
// Will exit with fatal error if there is an HTTP error (connection refused, etc)
func httpRequest(url string, method ...string) (int, string) {
	if len(method) == 0 {
		method = []string{"GET"}
	}

	client := &http.Client{}

	req, err := http.NewRequest(method[0], url, nil)
	if err != nil {
		log.Fatalf("HTTP Request Error: %s\n", err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("HTTP Client Error: %s\n", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("HTTP Read Error: %s\n", err)
	}

	return resp.StatusCode, string(body)
}

// getCurrentIP is a wrapper function to make an HTTP request to retrieve the current public IP Address and verifies
// that the returned string is a valid IPV IP Address. Will try a secondary or tertiary lookup if the primary fails.
// Returns a string
func getCurrentIP() string {
	ipList := [3]string{getIpUrl, getIpUrl2, getIpUrl3}

	for _, ipUrl := range ipList {
		result, ip := fetchParseIP(ipUrl)

		if result == true {
			return ip
		}
	}

	return ""
}

// fetchParse is a wrapper function, see getCurrentIP()
func fetchParseIP(host string) (bool, string) {
	if siteReachable(host) == true {
		_, body := httpRequest(getIpUrl)
		ip := strings.TrimSpace(body)

		if isIP(ip) {
			return true, ip
		}
	}

	return false, ""
}

// isIP expects a string to be passed, it will validate the string is a valid IP Address.
// Returns a bool
func isIP(ip string) bool {
	result := net.ParseIP(ip)
	if result == nil {
		return false
	}

	return true
}

// clientInfo returns GOOS and GOARCH information
func clientInfo() string {
	clientOS = runtime.GOOS
	clientArch = runtime.GOARCH
	var clientInfo = "Unknown"

	if clientOS != "" || clientArch != "" {
		clientInfo = clientOS + ";" + clientArch
	}

	return clientInfo
}

// setUserAgent builds the user-agent string for HTTP calls
func setUserAgent() string {
	clientInfo := clientInfo()

	return appName + "/" + appVersion + " (" + clientInfo + ") " + appCompany + " " + appUrl
}

// getMacAddressUuid retrieves the mac address of machine to generate a unique id.
// Skips virtual MAC addresses (Locally Administered Addresses).
// Returns uint64.
// See: https://gist.github.com/tsilvers/085c5f39430ced605d970094edf167ba
func getMacAddressUuid() uint64 {
	interfaces, err := net.Interfaces()
	if err != nil {
		return uint64(0)
	}

	for _, i := range interfaces {
		if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {

			if i.HardwareAddr[0]&2 == 2 {
				log.Printf("include virtual = false")
				continue
			}

			var macAddress uint64
			for j, b := range i.HardwareAddr {
				if j >= 8 {
					break
				}
				macAddress <<= 8
				macAddress += uint64(b)
			}

			return macAddress
		}
	}

	return uint64(0)
}
