#bin/sh
#
#
# Find all the go files in the current directory and compile for three platforms
files=`ls |grep go|grep -vi test`

# OSX
echo "Building for OSX\n"
go build -o builds/osx/dyndns ${files}

# Windows
echo "Building for Windows\n"
GOOS=windows GOARCH=386 go build -o builds/win/dyndns.exe ${files}

# Linux
echo "Building for Linux\n"
CC=x86_64-linux-musl-gcc CXX=x86_64-linux-musl-g++ GOARCH=amd64 GOOS=linux CGO_ENABLED=1 go build -ldflags "-linkmode external -extldflags -static" -o builds/linux/dyndns ${files}

output=`ls -alFR builds/|grep -v \/|grep -v DS`

echo "${output}"