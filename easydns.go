package main

import (
	"strings"
	"time"
)

const easydnsDnsAPI = "api.cp.easydns.com/dyn/generic.php"

// responseEvaleasydns evaluates the HTTP responses from provider DNS API
// Returns the response message from provider
func responseEvaleasydns(resp string, host Record) Record {
	//https://fusion.easydns.com/Knowledgebase/Article/View/102/7/dynamic-dns

	codes := []string{"NO_AUTH", "NO_SERVICE", "NO_PARTNER", "ERROR", "ILLEGAL", "TOO_FREQ", "OK"}
	host.LastCheckDate = time.Now()

	for _, code := range codes {
		if strings.Contains(resp, code) {
			switch code {
			case "NO_AUTH":
				host.Mesg = "The authentication failed. This no only happens if the username/password are wrong, but " +
					"if they send a hostname for a domain that isn’t under the user’s account."
				host.Wait = 0
				host.Stop = true
			case "NO_SERVICE":
				host.Mesg = "Dynamic DNS is not turned on for this domain. Each domain must have it’s own dyndns flag" +
					" enabled individually (by the user) in their member settings."
				host.Wait = 0
				host.Stop = true
			case "NO_PARTNER":
				host.Mesg = "Request did not include required partner information or there was an error detecting the " +
					"partner. This usually indicates there is something wrong with your request."
				host.Wait = 0
				host.Stop = true
			case "ERROR":
				host.Mesg = "Generic error encountered during request. This usually indicates there is something wrong " +
					"with your request or that the service is currently having issues."
				host.Wait = 0
				host.Stop = false
			case "ILLEGAL":
				host.Mesg = "Self-explanatory. Client sent data that is outside the allowed set for a dyndns update."
				host.Wait = 0
				host.Stop = true
			case "TOO_FREQ":
				host.Mesg = "Not enough time has elapsed since the last update. You need to wait at least 10 minutes " +
					"between updates."
				host.Wait = 600
				host.Stop = false
			case "OK":
				host.Mesg = "Everything works fine."
				host.Stop = false
				host.Wait = 0
				host.LastIPUpdate = time.Now()
				host.CurrentIP = currentIP
			}
		} else {
			host.Mesg = "Unrecognized response: " //+ resp
			host.Wait = 0
			host.Stop = true
		}
	}

	return host
}

// updateDNSeasydns calls provider API to update the DNS record with the current IP Address.
func UpdateDNSeasydns(data hostMap, host Record) Record {

	var u = strings.Join(data.Domain["username"], "")
	var p = strings.Join(data.Domain["password"], "")
	var h = strings.Join(data.Domain["hostname"], "")

	endpoint := "https://" + u + ":" + p + "@" + easydnsDnsAPI +
		"?hostname=" + h + "&myip=" + strings.TrimSpace(currentIP)

	statusCode, resp := httpRequest(endpoint, "POST")
	host.StatusCode = statusCode
	host = responseEvaleasydns(resp, host)

	return host
}
