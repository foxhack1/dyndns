# dyndns

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/alcastle/dyndns)](https://goreportcard.com/report/gitlab.com/alcastle/dyndns)
[![Go Coverage](https://img.shields.io/badge/coverage-82.6-green)](https://gocover.io/gitlab.com/alcastle/dyndns)
[![Go Doc](https://img.shields.io/badge/godoc-reference-blue)](https://godoc.org/gitlab.com/alcastle/dyndns)

![Runs on OSX](https://img.shields.io/badge/Runs%20on-OSX%20Catalina-brightgreen)
![Runs on Linux](https://img.shields.io/badge/Runs%20on-Linux-brightgreen)
![Runs on Windows](https://img.shields.io/badge/Runs%20on-Windows%2010%20Pro-green)

`dyndns` is s a simple app that updates dynamic DNS records for (one or many) domains, on a variety of providers. 
You can update multiple providers/domains at the same time with a simple config file.

How often it checks for IP changes is configurable, and only makes actual updates to the DNS provider 
if the IP has changed and requires updating. 

```
You can get started right away without needing Go by just downloading the pre-built 
executable for your operating system; the file you need is under the builds/ directory. 

For Windows you'll want this file
builds/win/dyndyns.exe 

Note: The config.yaml file needs to be in the same directory as the executable to work. 
Make sure to update the config.yaml with your information.
```

## Requirements
- Currently only tested on OSX 10.15.1 (Catalina), Windows 10 Pro, and Knoppix (8.2) Linux
- You must have an account from a supported provider:
  -  Google, easyDNS, No-IP, DYNU, or one that supports the Generic profile listed below.
- The executable and config.yaml need to be in the same directory.
- The directory needs to be writable.


## Configuration
- Update the `config.yaml` file adding in your own domains and credentials. See these pages for more information: 
- [Google](https://support.google.com/domains/answer/6147083?hl=en)
- [easyDNS](https://fusion.easydns.com/Knowledgebase/Article/View/102/7/dynamic-dns)
- [No-IP](https://www.noip.com/integrate/request)
- [DYNU](https://www.dynu.com/DynamicDNS/IP-Update-Protocol#ipupdate)
- Generic - allows you to add an unsupported provider so long as they support a basic auth API
```yaml
 - domain:
      provider: custom
      hostname: yourOtherOtherOther.domain.com
      username: domain5Username
      password: domain5Password
      customURL: api.custom.com
      method: POST # GET is also a valid option
      ## 
      # Replace values in quotes with the values the vendor is looking for, do not change the keys
      #
      ## 
      # The update was successful
      code_succes: "good"
      # There was no change - already has the correct IP Address
      code_noChange: "nochng"
      # No Host - the host does not exist or does not support Dynamic DNS
      code_noHost: "nohost"
      # Bad Auth - the username / password you've supplied is invalid
      code_badAuth: "badauth"
      # Bad Agent - The user agent is missing, wrong, or is blocked due to misuse.
      code_badAgent: "badagent"
      # Abuse - The user is blocked due to abuse. Too many requests, not following the spec or violation of TOS
      code_abuse: "abuse"
      # 911 - Something has gone wrong on the server-side, should wait 30 min to try again
      code_911: "911"

```


## Run
- The app only needs to be run once and will run periodically on it's own, based on the 
interval setting in the config.yaml file; `default is every 5 minutes`. From a terminal execute the file. 
##### On Linux and OSX
```
 $ ./dyndns
```
Alternatively you can run as a daemon by executing
```
 $ nohup ./dyndns &
```

##### On Windows
```
Open Command Prompt window and execute dyndns.exe
```
### Building from source
- You can find the latest build for OSX, Windows, and Linux ready to go in the builds directory.

**Build for OSX**
- If you want to make changes to the source code and rebuild on OSX. 
```
 $ go build -o builds/osx/dyndns main.go database.go utils.go counter.go easydns.go google.go no-ip.go custom.go dynu.go
```

**Build for Windows**
```
 $ GOOS=windows GOARCH=386 go build -o builds/win/dyndns.exe main.go database.go utils.go counter.go easydns.go google.go no-ip.go custom.go dynu.go
```

**Build for Linux**
- If you're building on OSX you'll need to install some extra packages
```
 $ brew install FiloScottile/musl-cross/musl-cross
 $ CC=x86_64-linux-musl-gcc CXX=x86_64-linux-musl-g++ GOARCH=amd64 GOOS=linux CGO_ENABLED=1 go build -ldflags "-linkmode external -extldflags -static" -o builds/linux/dyndns main.go database.go utils.go counter.go easydns.go google.go no-ip.go custom.go dynu.go
```

## Logs
- If you run with `nohup` informational and error logging will be written to nohup.out otherwise 
 they will be written to the terminal. 

## Force Update
To force all domains to be re-checked from scratch delete the database file `CI-DDNS.db`