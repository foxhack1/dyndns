package main

import (
	"strings"
	"time"
)

// responseEvalcustom evaluates the HTTP responses from provider DNS API
// Returns the response message from provider
func responseEvalcustom(resp string, host Record, data hostMap) Record {
	response := strings.TrimSpace(strings.ReplaceAll(resp, currentIP, ""))

	codeSuccess := strings.Join(data.Domain["codeSuccess"], "")
	codeNochg := strings.Join(data.Domain["codeNochg"], "")
	codeNohost := strings.Join(data.Domain["codeNohost"], "")
	codeBadAuth := strings.Join(data.Domain["codeBadAuth"], "")
	codeBadAgent := strings.Join(data.Domain["codeBadAgent"], "")
	codeAbuse := strings.Join(data.Domain["codeAbuse"], "")
	code911 := strings.Join(data.Domain["code911"], "")

	host.LastCheckDate = time.Now()

	switch response {
	case codeSuccess:
		host.Mesg = "DNS hostname update successful."
		host.Wait = 0
		host.Stop = false
		host.CurrentIP = currentIP
		host.LastIPUpdate = time.Now()
	case codeNochg:
		host.Mesg = "IP address is current, no update performed."
		host.Wait = 0
		host.Stop = false
		host.CurrentIP = currentIP
	case codeNohost:
		host.Mesg = "Hostname supplied does not exist under specified account, client exit and require user to " +
			"enter new login credentials before performing an additional request."
		host.Wait = 0
		host.Stop = true
	case codeBadAuth:
		host.Mesg = "Invalid username password combination."
		host.Wait = 0
		host.Stop = true
	case codeBadAgent:
		host.Mesg = "Client disabled. Client should exit and not perform any more updates without user intervention."
		host.Wait = 0
		host.Stop = true
	case codeAbuse:
		host.Mesg = "Username is blocked due to abuse. Either for not following our update specifications or " +
			"disabled due to violation of the No-IP terms of service. "
		host.Wait = 0
		host.Stop = true
	case code911:
		host.Mesg = "A fatal error on our side such as a database outage. Retry the update no sooner than 30 minutes."
		host.Wait = 1800
		host.Stop = false
	default:
		host.Mesg = "Unrecognized response: " + response
		host.Wait = 0
		host.Stop = true
	}

	return host
}

// UpdateDNScustom calls provider API to update the DNS record with the current IP Address.
func UpdateDNScustom(data hostMap, host Record) Record {

	var u = strings.Join(data.Domain["username"], "")
	var p = strings.Join(data.Domain["password"], "")
	var h = strings.Join(data.Domain["hostname"], "")
	var a = strings.Join(data.Domain["customURL"], "")
	var m = strings.Join(data.Domain["method"], "")

	// If auth isn't required by the provider
	var auth string = u + ":" + p + "@"
	if u == "" {
		auth = ""
	}

	endpoint := "https://" + auth + a +
		"?hostname=" + h + "&myip=" + strings.TrimSpace(currentIP)

	statusCode, resp := httpRequest(endpoint, m)
	host.StatusCode = statusCode
	host = responseEvalcustom(resp, host, data)

	return host
}
